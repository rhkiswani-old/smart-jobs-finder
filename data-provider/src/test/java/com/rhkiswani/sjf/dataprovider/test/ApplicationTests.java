package com.rhkiswani.sjf.dataprovider.test;
/**
 * Created by mohamed on 7/27/16.
 */

import com.rhkiswani.sjf.dataprovider.DataProviderApplication;
import com.rhkiswani.sjf.dataprovider.beans.*;
import com.rhkiswani.sjf.dataprovider.repos.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DataProviderApplication.class)
public class ApplicationTests {

    @Autowired
    private JobsRepository jobsRepository;

    @Test
    public void testInsert() {
        Job job = new Job();
//        job.setJobTitle("Kiswani");
//        job.setPostDate(new Date());
        try {
            jobsRepository.insert(job);
            assertEquals(jobsRepository.findAll().size(), 1);
        }catch (ConstraintViolationException e){
            System.out.println("------------------------------------------");
            Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                System.out.println(constraintViolation);
            }
        }

//        personRepository.deleteByJobTitle("Kiswani");
//        assertEquals(personRepository.findAll().size(), 0);
    }



}