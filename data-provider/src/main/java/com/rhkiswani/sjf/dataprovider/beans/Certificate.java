package com.rhkiswani.sjf.dataprovider.beans;

import org.springframework.data.annotation.Id;

/**
 * Created by mohamed on 7/27/16.
 */
public class Certificate extends SJFBean {
    @Id
    private int id;
    private String name;
    private String date;
    private Company company;
    private Country country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
