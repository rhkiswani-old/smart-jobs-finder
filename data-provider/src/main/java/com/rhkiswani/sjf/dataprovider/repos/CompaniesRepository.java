package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.City;
import com.rhkiswani.sjf.dataprovider.beans.Company;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CompaniesRepository extends SJFRepository<Company, Integer> {

}