package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Company;
import com.rhkiswani.sjf.dataprovider.beans.Country;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CountriesRepository extends SJFRepository<Country, Integer> {

}