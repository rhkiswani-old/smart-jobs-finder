package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Candidate;
import com.rhkiswani.sjf.dataprovider.beans.Job;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CandidatesRepository extends SJFRepository<Candidate, Integer> {

}