package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Location;
import com.rhkiswani.sjf.dataprovider.beans.Rank;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface RanksRepository extends SJFRepository<Rank, Integer> {

}