package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Rank;
import com.rhkiswani.sjf.dataprovider.beans.Skill;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface SkillsRepository extends SJFRepository<Skill, Integer> {

}