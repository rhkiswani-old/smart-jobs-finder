package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Certificate;
import com.rhkiswani.sjf.dataprovider.beans.City;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CitiesRepository extends SJFRepository<City, Integer> {

}