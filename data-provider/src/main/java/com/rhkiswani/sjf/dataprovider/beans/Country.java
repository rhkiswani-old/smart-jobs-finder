package com.rhkiswani.sjf.dataprovider.beans;

import org.springframework.data.annotation.Id;

/**
 * Created by mohamed on 7/27/16.
 */
public class Country extends SJFBean{

    @Id
    private int id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
