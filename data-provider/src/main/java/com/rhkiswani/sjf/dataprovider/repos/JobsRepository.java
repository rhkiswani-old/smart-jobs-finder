package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Job;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface JobsRepository extends SJFRepository<Job, Integer> {

    public void deleteByJobTitle(@Param("jobTitle") String jobTitle);

}