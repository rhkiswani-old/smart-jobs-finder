package com.rhkiswani.sjf.dataprovider.beans;

import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by mohamed on 7/27/16.
 */
public class Job extends SJFBean{
    @Id
    private String id;

    @NotNull(message = "Job Title is required")
    private String jobTitle;
    @NotNull(message = "Description is required")
    private String description;
    private Location location;
    @NotNull(message = "Start Date is required")
    private Date startDate;
    @NotNull(message = "End Date is required")
    private Date endDate;
    @NotNull(message = "Company is required")
    private Company company;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
