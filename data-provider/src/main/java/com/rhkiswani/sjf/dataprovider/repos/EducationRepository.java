package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Country;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.social.linkedin.api.Education;

@RepositoryRestResource
public interface EducationRepository extends SJFRepository<Education, Integer> {

}