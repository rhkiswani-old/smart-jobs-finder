package com.rhkiswani.sjf.dataprovider.beans;

import org.springframework.data.annotation.Id;

/**
 * Created by mohamed on 7/27/16.
 */
public class Industry extends SJFBean{
    @Id
    private int id;
    private String name;
    private int value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
