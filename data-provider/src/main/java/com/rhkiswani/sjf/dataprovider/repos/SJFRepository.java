package com.rhkiswani.sjf.dataprovider.repos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * Created by mohamed on 7/27/16.
 */
@NoRepositoryBean
public interface SJFRepository<T, ID extends Serializable> extends MongoRepository<T, ID> {
}
