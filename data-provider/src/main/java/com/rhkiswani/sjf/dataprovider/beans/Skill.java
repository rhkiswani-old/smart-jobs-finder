package com.rhkiswani.sjf.dataprovider.beans;

import org.springframework.data.annotation.Id;

/**
 * Created by mohamed on 7/27/16.
 */
public class Skill extends  SJFBean{
    @Id
    private int id;
    private String name;
    private Rank ranking;
    private Industry industry;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rank getRanking() {
        return ranking;
    }

    public void setRanking(Rank ranking) {
        this.ranking = ranking;
    }

    public Industry getIndustry() {
        return industry;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }
}
