package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Candidate;
import com.rhkiswani.sjf.dataprovider.beans.Certificate;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CertificatesRepository extends SJFRepository<Certificate, Integer> {

}