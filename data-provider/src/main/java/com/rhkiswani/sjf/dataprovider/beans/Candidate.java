package com.rhkiswani.sjf.dataprovider.beans;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by mohamed on 7/27/16.
 */
public class Candidate extends SJFBean{

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String currentJobTitle;
    private Collection<String> previousJobTitles;
    private Collection<String> keywords = new ArrayList<>();
    private Location currentLocation;
    private Education education;
    private String summary;
    private Collection<Certificate> certificates = new ArrayList<>();
    private Collection<Job> previousJobs = new ArrayList<>();
    private Collection<Skill> skills = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentJobTitle() {
        return currentJobTitle;
    }

    public void setCurrentJobTitle(String currentJobTitle) {
        this.currentJobTitle = currentJobTitle;
    }

    public Collection<String> getPreviousJobTitles() {
        return previousJobTitles;
    }

    public void setPreviousJobTitles(Collection<String> previousJobTitles) {
        this.previousJobTitles = previousJobTitles;
    }

    public Collection<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(Collection<String> keywords) {
        this.keywords = keywords;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Collection<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(Collection<Certificate> certificates) {
        this.certificates = certificates;
    }

    public Collection<Job> getPreviousJobs() {
        return previousJobs;
    }

    public void setPreviousJobs(Collection<Job> previousJobs) {
        this.previousJobs = previousJobs;
    }

    public Collection<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Collection<Skill> skills) {
        this.skills = skills;
    }
}
