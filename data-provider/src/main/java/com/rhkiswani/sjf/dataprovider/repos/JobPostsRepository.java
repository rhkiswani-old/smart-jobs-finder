package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.Industry;
import com.rhkiswani.sjf.dataprovider.beans.JobPost;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface JobPostsRepository extends SJFRepository<JobPost, Integer> {

}