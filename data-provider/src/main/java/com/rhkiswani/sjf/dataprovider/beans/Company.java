package com.rhkiswani.sjf.dataprovider.beans;

import org.springframework.data.annotation.Id;

/**
 * Created by mohamed on 7/27/16.
 */
public class Company extends SJFBean{
    @Id
    private int id;
    private String name;
    private Location location;
    private String contactPerson;
    private String contactEmail;
    private String jobsEmail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getJobsEmail() {
        return jobsEmail;
    }

    public void setJobsEmail(String jobsEmail) {
        this.jobsEmail = jobsEmail;
    }
}
