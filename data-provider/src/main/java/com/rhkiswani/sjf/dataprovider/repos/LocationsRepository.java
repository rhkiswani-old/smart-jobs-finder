package com.rhkiswani.sjf.dataprovider.repos;


import com.rhkiswani.sjf.dataprovider.beans.JobPost;
import com.rhkiswani.sjf.dataprovider.beans.Location;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface LocationsRepository extends SJFRepository<Location, Integer> {

}