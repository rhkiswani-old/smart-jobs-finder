package com.rhkiswani.sjf.loaderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class JobsLoaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobsLoaderApplication.class, args);
	}
}
