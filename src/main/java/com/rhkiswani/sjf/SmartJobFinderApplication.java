package com.rhkiswani.sjf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartJobFinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartJobFinderApplication.class, args);
	}
}
