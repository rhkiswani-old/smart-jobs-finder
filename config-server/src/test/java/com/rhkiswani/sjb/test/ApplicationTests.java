package com.rhkiswani.sjb.test;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import com.rhkiswani.sjb.ConfigServerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ConfigServerApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
public class ApplicationTests {

	@Value("${local.server.port}")
	private int port = 0;

	@Test
	public void configurationAvailable() {
		HttpMethod method = HttpMethod.GET;
		assertEquals(HttpStatus.OK, testEnv(method).getStatusCode());
	}

	@Test
	public void envPostAvailable() {
		HttpMethod method = HttpMethod.POST;
		assertEquals(HttpStatus.OK, testEnv(method).getStatusCode());
	}

	private ResponseEntity<Map> testEnv(HttpMethod method) {
		TestRestTemplate testRestTemplate = new TestRestTemplate();
		HttpEntity<String> request = new HttpEntity<String>(getAuthHeaders());
		ResponseEntity<Map> entity = testRestTemplate.exchange("http://localhost:" + port + "/env", method ,request, Map.class);
		return entity;
	}

	private HttpHeaders getAuthHeaders(){
		String plainCreds = "user:Mrk_9626";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		return headers;
	}

}