package com.test;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * Created by mohamed on 7/10/16.
 */
@SpringBootApplication
@EnableEurekaClient
public class Test {

    public static void main(String[] args) {
        new SpringApplicationBuilder(Test.class)
                .web(false)
                .run(args);
    }
}
